Exemple d'intégration continue en PHP
===================

Les scripts réalisent essentiellement rien. Des classes sont appelées et retournent
des valeurs.

À propos de phpunit
===================
https://phpunit.readthedocs.io/fr/latest/installation.html

phpunit est un dérivé de Xunit.

- l'installeur est un archive "php archive" (phar).
- composer: pour gérer les dépendances.
