<?php

require_once "./Select.classe.php";

use PHPUnit\Framework\TestCase;

class Test extends TestCase
{
    private $info;
    private $selection;

    public function testInfo()
    {
        //Hotfix à faire ici.
        //Modifié.
        $this->info = new Info("LoL");
        $this->assertEquals("LoL",  $this->info->getInfo());
    }

    public function testSelect()
    {
        $this->selection = new Select();
        $this->assertEquals(6,  $this->selection->selectCountNumEmpl());
    }

}

