
 create database bdtest;
 use bdtest;
 CREATE TABLE utilisateur (
  numeroEmploye int(10) NOT NULL primary key,
  MDP varchar(100) NOT NULL,
  nom varchar(100) NOT NULL,
  prenom varchar(100) NOT NULL
);

--
-- Contenu de la table `utilisateur`
--

INSERT INTO utilisateur (numeroEmploye, MDP, nom, prenom) VALUES
(121, '123', 'blow', 'joe');
INSERT INTO utilisateur (numeroEmploye, MDP, nom, prenom) VALUES
(122, '121', 'blow', 'joe');
INSERT INTO utilisateur (numeroEmploye, MDP, nom, prenom) VALUES
(123, '122', 'blow', 'joe');
INSERT INTO utilisateur (numeroEmploye, MDP, nom, prenom) VALUES
(124, '124', 'blow', 'joe');
INSERT INTO utilisateur (numeroEmploye, MDP, nom, prenom) VALUES
(125, '125', 'blow', 'joe');
INSERT INTO utilisateur (numeroEmploye, MDP, nom, prenom) VALUES
(126, '126', 'blow', 'joe');

CREATE USER 'php'@'localhost' IDENTIFIED BY 'password';
GRANT SELECT ON bdtest.utilisateur to 'php'@'localhost';
FLUSH PRIVILEGES;